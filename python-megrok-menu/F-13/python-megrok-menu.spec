%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname megrok.menu

Summary: Grok extension to configure browser menus
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 0.4
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/megrok.menu

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-grokcore-component
Requires: python-grokcore-security
Requires: python-grokcore-view
Requires: python-grokcore-viewlet
Requires: python-martian
Requires: python-setuptools
Requires: python-zope-browsermenu
Requires: python-zope-configuration
Requires: python-zope-publisher

%description
This package allows you to register browser menus and menu items for
browser views in Grok.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jul  9 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.4-1
- Initial packaging
