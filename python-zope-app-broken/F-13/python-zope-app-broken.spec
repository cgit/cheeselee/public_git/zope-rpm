%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.broken

Summary: Zope Broken (ZODB) Object Support
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.5.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://cheeseshop.python.org/pypi/zope.app.broken

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-security
Requires: python-zope-annotation
Requires: python-zope-broken
Requires: python-zope-app-appsetup
Requires: python-ZODB3

%description
When an object cannot be correctly loaded from the ZODB, this package allows
this object still to be instantiated, but as a "Broken" object. This allows
for gracefully updating the database without interruption.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Mon Jul  5 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.0-1
- Initial packaging
