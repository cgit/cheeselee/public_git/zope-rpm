%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname tempstorage
%global version 2.11.3
%global release 1

Summary: A RAM-based storage for ZODB
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/t/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/tempstorage

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-ZODB3

%description
This package is a storage implementation which uses RAM to persist objects, much
like MappingStorage. Unlike MappingStorage, it needs not be packed to get rid of
non-cyclic garbage and it does rudimentary conflict resolution.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
%{python_sitelib}/%{modname}/
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.11.3-1%{?dist}
- Initial packaging
