%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.principalannotation

Summary: Bootstrap subscriber and browser menu item for zope.principalannotation
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.7.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.principalannotation

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-transaction
Requires: python-zope-app-appsetup >= 3.11.0
Requires: python-zope-processlifetime
Requires: python-zope-principalannotation

%description
This package used to provide implementation of IAnnotations for zope.security
principal objects, but it's now moved to the zope.principalannotation
package. This package only contains a bootstrap subscriber that sets up
the principal annotation utility for the root site and the browser add
menu item for adding the annotation utility through ZMI.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jul  7 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.7.0-1
- Initial packaging
