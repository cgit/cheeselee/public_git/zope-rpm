%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.intid

Summary: Integer Id Utility
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.7.2
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.intid

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-ZODB3
Requires: python-zope-lifecycleevent >= 3.5.2
Requires: python-zope-component
Requires: python-zope-event
Requires: python-zope-interface
Requires: python-zope-keyreference
Requires: python-zope-location >= 3.5.4
Requires: python-zope-security

%description
This package provides an API to create integer ids for any object. Later
objects can be looked up by their id as well. This functionality is commonly
used in situations where dealing with objects is undesirably, such as in
search indices or any code that needs an easy hash of an object.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jul  7 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.7.2-1
- Initial packaging
