%include %{_rpmconfigdir}/macros.python
%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
%global modname ExtensionClass
%global version 2.13.2
%global release 1

Summary: Metaclass for subclassable extension types
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/E/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/ExtensionClass

BuildRequires: python2-devel
BuildRequires: python-setuptools

%description
ExtensionClass and ExtensionClass-related packages
==================================================

ExtensionClass
--------------

This package provides a metaclass that allows classes implemented in
extension modules to be subclassed in Python.  Unless you need
ExtensionClasses for legacy applications (e.g. Zope 2), you probably
want to use Python's new-style classes (available since Python 2.2).

ComputedAttribute
-----------------

This package provides a way to attach attributes to an
ExtensionClass or instance that are computed by calling a
callable.  This works very much like property known from new-style
classes, except that a ComputedAttribute can also be attached to
an instance and that it honors ExtensionClass semantics (which is
useful for retaining Acquisition wrappers, for example).

MethodObject
------------

This package lets you attach additional "methods" to ExtensionClasses.
These "methods" are actually implemented by subclassing the
MethodObject.Method class and implementing the __call__ method
there.  Instances of those classes will be bound to the instances
they're attached to and will receive that instance object as a first
parameter (after self).

%package devel
Summary:  Developer files for %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: python2-devel
BuildArch: noarch
%description devel
Files for developing applications using %{name}.

%prep
%setup -q -n %{modname}-%{version}

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# remove contained source file(s)
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv
# deal with header file(s)
mkdir -p $RPM_BUILD_ROOT%{_includedir}/python%{py_ver}/%{modname}/
mv $RPM_BUILD_ROOT%{python_sitearch}/%{modname}/%{modname}.h \
   $RPM_BUILD_ROOT%{_includedir}/python%{py_ver}/%{modname}/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt  COPYRIGHT.txt  LICENSE.txt README.txt
%{python_sitearch}/%{modname}/
%{python_sitearch}/%{modname}-*.egg-info
%{python_sitearch}/ComputedAttribute/
%{python_sitearch}/MethodObject/

%files devel
%defattr(-,root,root,-)
%{_includedir}/python%{py_ver}/%{modname}/

%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.13.2-1
- Initial packaging
