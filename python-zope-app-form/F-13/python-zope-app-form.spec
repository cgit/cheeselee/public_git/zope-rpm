%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.form
%global version 4.0.2
%global release 1

Summary: The Original Zope 3 Form Framework
Name: python-zope-app-form
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.form

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-transaction
Requires: python-zope-formlib
Requires: python-zope-browser
Requires: python-zope-browserpage
Requires: python-zope-browsermenu
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-datetime
Requires: python-zope-exceptions
Requires: python-zope-i18n
Requires: python-zope-interface
Requires: python-zope-proxy
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-security

%description
This package provides the old form framework for Zope 3. It also
implements a few high-level ZCML directives for declaring forms. More
advanced alternatives are implemented in zope.formlib and
z3c.form. The widgets that were defined in here were moved to
zope.formlib. Version 4.0 and newer are maintained for backwards
compatibility reasons only.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{python_sitelib}/zope/app/form/browser/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
%doc src/zope/app/form/browser/*.txt
%{python_sitelib}/zope/app/form/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 4.0.2-1
- Initial packaging
