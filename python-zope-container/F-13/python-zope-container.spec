%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
%global modname zope.container
%global version 3.11.1
%global release 2

Summary: Zope Container
Name: python-zope-container
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/zope.container

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-interface
Requires: python-zope-dottedname
Requires: python-zope-schema
Requires: python-zope-component
Requires: python-zope-event
Requires: python-zope-location
Requires: python-zope-security
Requires: python-zope-lifecycleevent
Requires: python-zope-i18nmessageid
Requires: python-zope-filerepresentation
Requires: python-zope-size
Requires: python-zope-traversing
Requires: python-zope-publisher
Requires: python-zope-broken
Requires: python-ZODB3

%description
This package define interfaces of container components, and provides
container implementations such as a BTreeContainer and
OrderedContainer, as well as the base class used by zope.site.folder
for the Folder implementation.

%prep
%setup -q -n %{modname}-%{version}

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# remove contained source file(s)
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitearch}/zope/container/
%{python_sitearch}/%{modname}-*.egg-info
%{python_sitearch}/%{modname}-*-nspkg.pth


%changelog
* Tue Jun 22 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.11.1-2
- don't move the text files

* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.11.1-1
- Initial packaging
