%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zc.buildout

Summary: System for managing development buildouts
Name: python-%(echo %{modname} | sed -r 's|\.|-|')
Version: 1.5.1
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://buildout.org

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-setuptools

%description
The Buildout project provides support for creating applications,
especially Python applications.  It provides tools for assembling
applications from multiple parts, Python or otherwise.  An application
may actually contain multiple programs, processes, and configuration
settings.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt todo.txt DEVELOPERS.txt SYSTEM_PYTHON_HELP.txt doc/* specifications/
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|')
%dir %{python_sitelib}/zc/
%{_bindir}/buildout
%exclude %{_usr}/README.txt
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Sep  1 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.5.1-1
- Update to 1.5.1
- Own %%{python_sitelib}/zc/
- Remove python-zope-filesystem from requirements

* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.4.3-1
- Initial packaging
