%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname z3c.autoinclude

Summary: Automatically include ZCML
Name: python-%(echo %{modname} | sed -r 's|\.|-|')
Version: 0.3.3
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/%{modname}/

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-dottedname
Requires: python-zope-interface
Requires: python-zope-configuration
Requires: python-zope-schema
Requires: python-zc-buildout

%description
This package adds two new ZCML directives to automatically detect 
ZCML files to include: "includeDependencies" and "includePlugins".

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt TODO.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|')
%{_bindir}/autoinclude-test
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.3.3-1
- Initial packaging
