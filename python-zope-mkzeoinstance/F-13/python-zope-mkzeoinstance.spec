%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.mkzeoinstance
%global version 3.9.4
%global release 2

Summary: Make standalone ZEO database server instances
Name: python-zope-mkzeoinstance
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.mkzeoinstance

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
#Requires: python-setuptools
Requires: python-ZODB3

%description
This package provides a single script, ``mkzeoinstance``, which creates
a standalone ZEO server instance.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
%{_bindir}/mkzeoinstance
%{python_sitelib}/zope/mkzeoinstance/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth

%changelog
* Sat Jul  3 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.4-2
- Include %%{_bindir}/mkzeoinstance

* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.4-1
- Initial packaging
