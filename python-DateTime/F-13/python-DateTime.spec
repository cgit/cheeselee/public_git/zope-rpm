%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname DateTime

Summary: This package provides a DateTime data type from Zope 2
Name: python-%{modname}
Version: 2.12.3
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/D/%{modname}/%{modname}-%{version}.zip
BuildArch: noarch
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/DateTime

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: pytz
Requires: python-zope-interface

%description
This package provides a DateTime data type, as known from Zope 2.
Unless you need to communicate with Zope 2 APIs, you're probably
better off using Python's built-in datetime module.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt  COPYRIGHT.txt  LICENSE.txt README.txt
%{python_sitelib}/%{modname}/
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Sat Jul 10 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.12.3-1
- Update to 2.12.3

* Tue Jun 15 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.12.2-1
- Initial packaging
