%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grokproject

Summary: Script that sets up a grok project directory
Name: grokproject
Version: 2.0.1
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/g/%{modname}/%{modname}-%{version}.tar.gz
# Patch to make it work without running buildout
Patch0: grokproject-2.0.1-work-without-buildout.patch
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: https://launchpad.net/grok

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-paste-script > 1.6
# not listed by upstream
Requires: python-setuptools
# from the template
Requires: python-setuptools
Requires: python-grok
Requires: python-grokui-admin
Requires: python-z3c-testsetup
Requires: python-grokcore-startup
Requires: python-z3c-recipe-i18n

%description
grokproject provides an easy way to get started with a Grok application.

%prep
%setup -q -n %{modname}-%{version}
%patch0 -p1
chmod a+x grokproject/template/bin/*

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# make some directories in the template
mkdir -p $RPM_BUILD_ROOT%{python_sitelib}/%{modname}/template/{parts/test,var/{filestorage,blobstoage}}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt upgrading.txt README-shorttests.txt README.txt tests_alternative_release_url.txt TODO.txt tests_paste.txt
%{_bindir}/%{name}
%{python_sitelib}/%{modname}/
# exclude some empty files
%exclude %{python_sitelib}/%{modname}/template/log/.directory
%exclude %{python_sitelib}/%{modname}/template/extends-cache/.directory
# exclude byte-compiled files in the template
%exclude %{python_sitelib}/%{modname}/template/bootstrap.pyo
%exclude %{python_sitelib}/%{modname}/template/bootstrap.pyc
%exclude %{python_sitelib}/%{modname}/template/src/+package+/__init__.pyo
%exclude %{python_sitelib}/%{modname}/template/src/+package+/__init__.pyc
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.0.1-1
- Initial packaging
