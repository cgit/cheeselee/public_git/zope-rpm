%define python_minver 2.6
%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%endif

%define zope_user      zope
%define zope_group     %{zope_user}

%define zope_home      %{_libdir}/zope
%define software_home  %{zope_home}/lib/python
%define instance_home  %{_localstatedir}/lib/zope

%define zopectl        %{_bindir}/zopectl
%define runzope        %{_bindir}/runzope

Name:    zope
Summary: Web application server for flexible content management applications
Version: 2.12.8
Release: 1%{?dist}
License: ZPLv2.1
Group: 	 System Environment/Daemons
URL:     http://www.zope.org/
Source0: http://pypi.python.org/packages/source/Z/Zope2/Zope2-%{version}.zip
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source1: zope.init.in
Source2: zope.sysconfig.in
Source4: zope-README.Fedora
Source5: zope.logrotate.in
Source6: zope.logrotate.cron.in
Patch0: zope-2.12.7-config.patch

BuildRequires: python-devel >= %{python_minver}
BuildRequires: python-setuptools
BuildRequires: python-sphinx
Requires: python-Acquisition
Requires: python-DateTime
Requires: python-ExtensionClass
Requires: python-Missing
Requires: python-MultiMapping
Requires: python-Persistence
Requires: python-Record
Requires: python-RestrictedPython
Requires: python-ThreadLock
Requires: python-ZConfig
Requires: python-ZODB3
Requires: python-ZopeUndo
Requires: python-docutils
Requires: python-five-formlib
Requires: python-initgroups
Requires: pytz
Requires: python-setuptools
Requires: python-tempstorage
Requires: python-transaction
Requires: python-zdaemon
Requires: python-zLOG
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-container
Requires: python-zope-contentprovider
Requires: python-zope-contenttype
Requires: python-zope-deferredimport
Requires: python-zope-event
Requires: python-zope-exceptions
Requires: python-zope-i18n 
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-lifecycleevent
Requires: python-zope-location
Requires: python-zope-mkzeoinstance
Requires: python-zope-pagetemplate
Requires: python-zope-processlifetime
Requires: python-zope-proxy
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-sendmail < 3.7.0
Requires: python-zope-sequencesort
Requires: python-zope-site
Requires: python-zope-size
Requires: python-zope-structuredtext
Requires: python-zope-tal
Requires: python-zope-tales
Requires: python-zope-testbrowser 
Requires: python-zope-testing
Requires: python-zope-traversing
Requires: python-zope-viewlet
Requires: python-zope-app-publication
Requires: python-zope-app-publisher
Requires: python-zope-app-schema
# extra in zope.testbrowser
Requires: python-zope-app-testing

Requires(pre): shadow-utils
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig, /sbin/service
Requires(postun): /sbin/service

%description
Zope is an application server framework that enables developers to quickly
build web applications such as intranets, portals, and content management
systems.

Zope, by default, will listen on port 8080.

%prep
%setup -q -n Zope2-%{version}
%patch0 -p0

sed -i -e '/^#!/, 1d' src/ZPublisher/{Test,Client}.py \
    src/Products/PageTemplates/tests/run.py \
    src/Products/ZCTextIndex/tests/{wordstats,hs-tool,indexhtml}.py \
    src/Zope2/Startup/{misc/zpasswd,zopectl}.py \
    src/DocumentTemplate/release.sh \
    src/ZServer/medusa/{test/asyn_http_bench.py,http_server.py} \
    src/ZTUtils/tests/run.py src/ZPublisher/Client.py src/OFS/ndiff.py
    
chmod -x src/AccessControl/securitySuite/regressionSecurity.py
chmod -x src/Zope2/utilities/skel/import/README.txt
install -pm 644 %{SOURCE4} README.Fedora
# don't include batch files
rm -f src/Zope2/utilities/skel/bin/*.bat*

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

# make html documents
pushd doc
make html
rm -fr .build/html/{_sources,.buildinfo}
popd


%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT \
       --install-scripts=%{_bindir} --install-lib=%{software_home}

# add a missed namespace holder
cp -p src/Products/__init__.py $RPM_BUILD_ROOT%{software_home}/Products/

# delete included C source files
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv

# Create all required additional directories
for dir in %{instance_home}/{Products,bin,var} %{_sysconfdir}/sysconfig; do
    install -d $RPM_BUILD_ROOT$dir
done


install -D -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_initddir}/zope
install -D -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/zope
install -D -m 644 %{SOURCE5} \
    $RPM_BUILD_ROOT%{software_home}/Zope2/utilities/skel/etc/logrotate.conf.in
install -D -m 755 %{SOURCE6} $RPM_BUILD_ROOT%{_sysconfdir}/cron.daily/zope-logrotate
perl -pi -e 's,<<SYSCONFDIR>>,%{_sysconfdir},g;
             s,<<BINDIR>>,%{_bindir},g;
             s,<<LOCALSTATEDIR>>,%{_localstatedir},g;
             s,<<ZOPE_USER>>,%{zope_user},g' \
    $RPM_BUILD_ROOT%{_initddir}/zope \
    $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/zope \
    $RPM_BUILD_ROOT%{_sysconfdir}/cron.daily/zope-logrotate \
    README.Fedora

# Install the skel, translating paths, into the build root
%{__python} "src/Zope2/utilities/copyzopeskel.py" \
     --sourcedir="$RPM_BUILD_ROOT%{software_home}/Zope2/utilities/skel" \
     --targetdir="$RPM_BUILD_ROOT%{instance_home}" \
     --replace="INSTANCE_HOME:%{instance_home}" \
     --replace="SOFTWARE_HOME:%{software_home}" \
     --replace="ZOPE_HOME:%{zope_home}" \
     --replace="PYTHON:%{__python}" \
     --replace="ZOPE_SCRIPTS:%{_bindir}"

chmod 750 $RPM_BUILD_ROOT%{instance_home}

# Fix permissions, must have changed in the upstream tar
chmod 755 $RPM_BUILD_ROOT%{instance_home}/bin/zopectl
chmod 755 $RPM_BUILD_ROOT%{instance_home}/bin/runzope

# Set needed permissions
# We might go as far as to only allow zope r/w to the .pyc files
for dir in %{instance_home}/{Products,log,lib,var}; do
    chmod 775 $RPM_BUILD_ROOT$dir
done

chmod 755 $RPM_BUILD_ROOT%{zope_home}

# write version.txt
echo "Zope %{version}-%{release}" > \
    "$RPM_BUILD_ROOT%{software_home}/version.txt"

# write Zope2.pth
install -d $RPM_BUILD_ROOT%{python_sitearch}
echo "%{software_home}" > \
    "$RPM_BUILD_ROOT%{python_sitearch}/Zope2.pth"

%clean
rm -rf $RPM_BUILD_ROOT


%pre
getent group %{zope_group} >/dev/null || groupadd -r %{zope_group}
getent passwd %{zope_user} >/dev/null || \
  useradd -r -g %{zope_group} -d %{zope_home} -s /sbin/nologin \
    -c "Zope user" %{zope_user}
exit 0


%post
# add zope init to runlevels
/sbin/chkconfig --add zope


%preun
if [ $1 -eq 0 ]; then
  /sbin/service zope stop >/dev/null 2>&1
  /sbin/chkconfig --del zope
fi

%postun
if [ "$1" -ge "1" ] ; then
    /sbin/service zope condrestart >/dev/null 2>&1 || :
fi


%files 
%defattr(-, root, root, -)
%doc COPYRIGHT.txt LICENSE.txt README.txt doc/.build/html README.Fedora
%config(noreplace) %{_sysconfdir}/sysconfig/zope
%{_initddir}/zope
%config %{_sysconfdir}/cron.daily/zope-logrotate
%{_bindir}/zopectl
%exclude %{_bindir}/mkzeoinstance
%{_bindir}/mkzopeinstance
%{_bindir}/runzope
%{_bindir}/zpasswd
%dir %{zope_home}
%{zope_home}/lib
%attr(-, root, %{zope_group}) %{instance_home}/
%{python_sitearch}/Zope2.pth


%changelog
* Sat Jul  3 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.12.8-1
- Update to 2.12.8
- Use %%{_bindir}/mkzeoinstance from python-zope-mkzeoinstance

* Fri Jun 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.12.7-1
- Update to 2.12.7
- License Specified to ZPLv2.1
- Source0 URL renewed
- zope-2.10.4-config.patch and zope-2.10.8-configure.patch removed,
  zope-2.12.7-config.patch added to make the same effect of
  zope-2.10.4-config.patch
- BR: python-setuptools and python-sphinx added
- Add multiple requirements
- zope.zopectl.in removed
- %%pre rewritten and %%postun added following
  http://fedoraproject.org/wiki/Packaging/SysVInitScript
- zope.init.in renewed
- zope.logrotate.in included in the binary package
- Don't include the batch files

* Thu Aug 6 2009 Jonathan Steffan <jon a fedoraunity.org> 2.10.9-1
- Update to 2.10.9
- Fix CVE-2009-0669 (BZ#513428)
- Fix CVE-2009-0668 (BZ#513422)

* Mon May 25 2009 Jonathan Steffan <jon a fedoraunity.org> 2.10.8-1
- Update to 2.10.8

* Sat Oct 25 2008 Jonathan Steffan <jon a fedoraunity.org> 2.10.7-1
- Update to 2.10.7

* Sun May 11 2008 Jonathan Steffan <jon a fedoraunity.org> 2.10.6-1
- Update to 2.10.6
- Add a patch to allow python 2.4.3

* Thu Nov 8 2007 Jonathan Steffan <jon a fedoraunity.org> 2.10.5-2
- Update permissions for zopectl and runzope

* Sat Nov 3 2007 Jonathan Steffan <jon a fedoraunity.org> 2.10.5-1
- Update to zope 2.10.5

* Mon Sep 3 2007 Jonathan Steffan <jon a fedoraunity.org> 2.10.4-3
- Updated Requires for libxml2-python and python-elementtree

* Tue Aug 14 2007 Jonathan Steffan <jon a fedoraunity.org> 2.10.4-2
- Added config patch

* Wed Aug 1 2007 Jonathan Steffan <jon a fedoraunity.org> 2.10.4-1
- Initial Package

