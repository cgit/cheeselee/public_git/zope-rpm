%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zc.recipe.egg

Summary: Recipe for installing Python package distributions as eggs
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 1.2.2
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://cheeseshop.python.org/pypi/zc.recipe.egg

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem >= 1-10
Requires: python-zc-buildout >= 1.2.0
Requires: python-setuptools

%description
The egg-installation recipe installs eggs into a buildout eggs
directory.  It also generates scripts in a buildout bin directory with 
egg paths baked into them.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sat Jul 10 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.2.2-1
- Initial packaging
