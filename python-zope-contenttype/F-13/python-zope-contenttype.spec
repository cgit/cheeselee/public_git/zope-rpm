%global modname zope.contenttype

Summary: Content-Type Handling Utility
Name: python-zope-contenttype
Version: 3.5.1
Release: 2%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.contenttype

# register the test suite, applied in upstream svn116335 with trivial difference
Patch0: python-zope-contenttype-3.5.1-run-tests.patch

BuildRequires: python2-devel
BuildRequires: python-setuptools

%description
This package provides a utility module for content-type handling.

%prep
%setup -q -n %{modname}-%{version}
%patch0 -p1 -b .run_tests

%build
%{__python} setup.py build

%install
%{__python} setup.py install --root=$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/contenttype/
%dir %{python_sitelib}/zope/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth

%check
%{__python} setup.py test

%changelog
* Sun Sep 12 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.1-2
- Requires: python-zope-filesystem and python-setuptools removed
- Spec cleaned up
- Co-own %%{python_sitelib}/zope/
- Add %%check section and run tests
- Add python-zope-contenttype-3.5.1-run-tests.patch to register the test_suite

* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.1-1
- Initial packaging
