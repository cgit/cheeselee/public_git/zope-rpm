%global modname zope.datetime

Summary: Zope datetime utilities
Name: python-zope-datetime
Version: 3.4.0
Release: 3%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://svn.zope.org/zope.datetime
Patch0: python-zope-datetime-3.4.0-run_tests.patch

BuildRequires: python2-devel
BuildRequires: python-setuptools
# for tests
BuildRequires: python-zope-testing

%description
This package provides commonly used date and time related utility functions.

%prep
%setup -q -n %{modname}-%{version}
%patch0 -p1 -b .run_tests

%build
%{__python} setup.py build

%install
%{__python} setup.py install --root=%{buildroot}

%check
%{__python} setup.py test

%files
%defattr(-,root,root,-)
# LICENSE will be included in next upstream release.
%doc README.txt
# Co-own %%{python_sitelib}/zope/
%dir %{python_sitelib}/zope/
%{python_sitelib}/zope/datetime/
%exclude %{python_sitelib}/zope/datetime/tests/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sat Sep 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.0-3
- Co-own %%{python_sitelib}/zope/

* Sat Sep 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.0-2
- Requires: python-zope-filesystem and python-setuptools removed
- Spec cleaned up
- Exlude the tests from installation
- Add a small patch and %%check section and run tests
- BR: python-zope-testing added for tests

* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.0-1
- Initial packaging
