%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.wsgi

Summary: WSGI application for the zope.publisher
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.9.2
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.wsgi

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-ZConfig
Requires: python-wsgi_intercept
Requires: python-ZODB3
Requires: python-zope-app-appsetup >= 3.14.0
Requires: python-zope-processlifetime
Requires: python-zope-app-publication
Requires: python-zope-event
Requires: python-zope-interface
Requires: python-zope-publisher
Requires: python-zope-security
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-container
Requires: python-zope-error
Requires: python-zope-lifecycleevent
Requires: python-zope-session
Requires: python-zope-site
Requires: python-zope-testbrowser
Requires: python-zope-testing
Requires: python-zope-traversing

%description
This package provides the 'WSGIPublisherApplication' class which
exposes the object publishing machinery in 'zope.publisher' as a
WSGI application.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jul  7 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.2-1
- Initial packaging
