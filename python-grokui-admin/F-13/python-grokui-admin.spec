%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grokui.admin

Summary: The Grok administration and development UI
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 0.7.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://svn.zope.org/grokui.admin

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-ZODB3
Requires: python-grok
Requires: python-grokui-base
Requires: python-megrok-layout >= 1.0.1
Requires: python-setuptools
Requires: python-z3c-flashmessage
Requires: python-zope-applicationcontrol
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-contentprovider
Requires: python-zope-exceptions
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-schema
Requires: python-zope-site
Requires: python-zope-size
Requires: python-zope-traversing

%description
grokui.admin: A basic grok admin UI

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jul  8 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.7.0-1
- Initial packaging
