%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname RestrictedPython
%global version 3.5.2
%global release 1

Summary: A restricted execution environment for Python
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/R/%{modname}/%{modname}-%{version}.zip
BuildArch: noarch
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/RestrictedPython

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-setuptools

%description
RestrictedPython provides a restricted_compile function that works
like the built-in compile function, except that it allows the
controlled and restricted execution of code.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt COPYRIGHT.txt LICENSE.txt README.txt
%{python_sitelib}/%{modname}/
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Tue Jun 15 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.2-1
- Initial packaging
