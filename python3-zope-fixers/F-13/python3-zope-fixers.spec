%global modname zope.fixers

Summary: 2to3 fixers for Zope
Name: python3-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 1.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://svn.zope.org/zope.fixers/

BuildRequires: python3-devel
BuildRequires: python3-setuptools


%description
Fixers for Zope Component Architecture and the frameworks built with it.

Currently, there is only one fixer, fix_implements. This fixer will change
all uses of implements(IFoo) in a class body to the class decorator
@implementer(IFoo), which is the most likely Python 3 syntax for 
zope.interfaces implements statements.

%prep
%setup -q -n %{modname}-%{version}

%build
%{__python3} setup.py build

%install
%{__python3} setup.py install --root=$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt ZPL.txt docs/*
%{python3_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%dir %{python3_sitelib}/zope/
%{python3_sitelib}/%{modname}-*.egg-info
%{python3_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Sep  2 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.0-1
- Initial packaging
