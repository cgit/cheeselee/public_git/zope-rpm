%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname z3c.testsetup

Summary: Easier test setup for Zope 3 projects and other Python packages
Name: python-%(echo %{modname} | sed -r 's|\.|-|')
Version: 0.7
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/z3c.testsetup

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-testing
Requires: python-martian

%description
z3c.testsetup: easy test setup for zope 3 and python projects

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc DEVELOPMENT.txt LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.7-1
- Initial packaging
