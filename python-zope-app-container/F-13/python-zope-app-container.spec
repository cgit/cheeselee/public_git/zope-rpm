%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.container

Summary: Zope Container
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.8.2
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://cheeseshop.python.org/pypi/zope.app.container

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-browser
Requires: python-zope-component
Requires: python-zope-container
Requires: python-zope-copypastemove
Requires: python-zope-dublincore
Requires: python-zope-event
Requires: python-zope-exceptions
Requires: python-zope-i18n
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-lifecycleevent
Requires: python-zope-location
Requires: python-zope-publisher >= 3.12
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-size
Requires: python-zope-traversing
Requires: python-zope-app-publisher

%description
This package define interfaces of container components, and provides
sample container implementations such as a BTreeContainer and
OrderedContainer.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Tue Jul  6 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.8.2-1
- Initial packaging
