%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.testing
%global version 3.7.5
%global release 1

Summary: Zope Application Testing Support
Name: python-zope-app-testing
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.testing

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-annotation
Requires: python-zope-app-appsetup
Requires: python-zope-processlifetime
Requires: python-zope-app-debug
Requires: python-zope-app-dependable
Requires: python-zope-app-publication
Requires: python-zope-component
Requires: python-zope-container
Requires: python-zope-i18n
Requires: python-zope-interface
Requires: python-zope-password
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-site
Requires: python-zope-testing
Requires: python-zope-traversing

%description
This package provides testing support for Zope 3 applications. Besides
providing numerous setup convenience functions, it implements a testing setup
that allows the user to make calls to the publisher allowing to write
functional tests.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
#rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/app/testing/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
#%doc src/zope/app/testing/*.txt
%{python_sitelib}/zope/app/testing/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jun 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.7.5-1
- Initial packaging
