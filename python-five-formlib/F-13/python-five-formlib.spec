%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname five.formlib
%global version 1.0.3
%global release 1

Summary: zope.formlib integration for Zope 2
Name: python-five-formlib
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/f/%{modname}/%{modname}-%{version}.zip
BuildArch: noarch
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/five.formlib

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-transaction
Requires: python-zope-app-form
Requires: python-zope-browser
Requires: python-zope-component
Requires: python-zope-event
Requires: python-zope-formlib
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-lifecycleevent
Requires: python-zope-location
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-ExtensionClass
Requires: zope

%description
five.formlib provides integration of the zope.formlib and zope.app.form
packages into the Zope2 application server.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/five/formlib/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth



%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.0.3-1
- Initial packaging
