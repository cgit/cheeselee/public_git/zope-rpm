%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
%global modname zope.security
%global version 3.7.3
%global release 2

Summary: Zope Security Framework
Name: python-zope-security
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/zope.security

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-proxy
Requires: python-zope-schema

%description
The Security framework provides a generic mechanism to implement security
policies on Python objects.

%prep
%setup -q -n %{modname}-%{version}
sed -i -e '/^#! *\//, 1d' src/zope/security/setup.py

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# remove contained source file(s)
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitearch}/zope/security/
%{python_sitearch}/%{modname}-*.egg-info
%{python_sitearch}/%{modname}-*-nspkg.pth


%changelog
* Tue Jun 22 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.7.3-2
- don't move the text files
- Requires: python-setuptools added

* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.7.3-1
- Initial packaging
