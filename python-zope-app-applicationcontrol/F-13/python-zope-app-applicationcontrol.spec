%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.applicationcontrol

Summary: Zope applicationcontrol
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.5.7
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.applicationcontrol

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-applicationcontrol
Requires: python-zope-component
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-security
Requires: python-zope-size
Requires: python-zope-traversing >= 3.7.0

%description
The application control instance is usually generated upon startup.
This package provides runtime information adapter for application
control and Zope version.  Also provide a utility with methods for
shutting down and restarting the server.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jul  8 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.7-1
- Initial packaging
