%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.testbrowser
%global version 3.9.0
%global release 1

Summary: Programmable browser for functional black-box tests
Name: python-zope-testbrowser
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.testbrowser

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-mechanize > 0.2.0
Requires: python-zope-interface
Requires: python-zope-schema
Requires: pytz

%description
zope.testbrowser provides an easy-to-use programmable web browser
with special focus on testing.  It is used in Zope, but it's not Zope
specific at all.  For instance, it can be used to test or otherwise
interact with any web site.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/testbrowser/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt COPYRIGHT.txt
%doc src/zope/testbrowser/*.txt
%{python_sitelib}/zope/testbrowser/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.0-1
- Initial packaging
