%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grokcore.view

Summary: Grok-like configuration for Zope browser pages
Name: python-%(echo %{modname} | sed -r 's|\.|-|')
Version: 1.13.5
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://grok.zope.org

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-setuptools
Requires: python-grokcore-component >= 1.5
Requires: python-grokcore-security >= 1.2
Requires: python-martian >= 0.10
Requires: python-zope-browserresource >= 3.9.0
Requires: python-zope-component
Requires: python-zope-interface
Requires: python-zope-login
Requires: python-zope-pagetemplate
Requires: python-zope-ptresource >= 3.9.0
Requires: python-zope-publisher
Requires: python-zope-security
Requires: python-zope-traversing

%description
This package provides support for writing browser pages for Zope
and registering them directly in Python (without ZCML).

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt TODO.txt CREDITS.txt COPYRIGHT.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.13.5-1
- Initial packaging
