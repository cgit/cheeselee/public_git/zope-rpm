%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grokui.base

Summary: The Grok administration and development UI (base)
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 0.2.2
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://svn.zope.org/grokui.base

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-grokcore-component
Requires: python-grokcore-message
Requires: python-grokcore-view
Requires: python-grokcore-viewlet
Requires: python-megrok-layout
Requires: python-megrok-menu
Requires: python-setuptools
Requires: python-zope-authentication
Requires: python-zope-browsermenu
Requires: python-zope-component
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-publisher
Requires: python-zope-site
Requires: python-zope-security
Requires: python-zope-traversing

%description
grokui.base is a base layer to build a zope instance-level set of
utilities. The package provides a collection of easy-to-use components
that will allow you to build your own configuration or admin panels.
grokui.base provides the components that should be used by other
grokui packages to plug into a coherent layout.

Using grokui.base we can provide different UI parts that can be used
independently from each other, for example a ZODB browser or a
general admin panel to manage local Grok applications. It is up to the
admins to decide what grok UI parts they want to have installed.

In general, grokui.base provides viewlets, menus, layouts and a
special namespace for use by other components.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jul  9 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.2.2-1
- Initial packaging
