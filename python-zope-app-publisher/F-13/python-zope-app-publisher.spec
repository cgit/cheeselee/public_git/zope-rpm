%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.publisher
%global version 3.10.1
%global release 1

Summary: Implementations and means for configuration of Zope3-style views and resources
Name: python-zope-app-publisher
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.publisher/

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-browsermenu
Requires: python-zope-browserpage
Requires: python-zope-browserresource
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-datetime
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-ptresource
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-componentvocabulary

%description
This package used to provide browser page, resource and menu classes
for use with zope.publisher object publishing framework, as well as some
other useful utilities and adapters, but most of things was factored out
to separate packages, leaving here only backward-compatibility imports.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/app/publisher/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.10.1-1
- Initial packaging
