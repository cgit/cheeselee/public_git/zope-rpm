%global modname zope.sequencesort

Summary: Sequence Sorting
Name: python-zope-sequencesort
Version: 3.4.0
Release: 2%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://cheeseshop.python.org/pypi/zope.sequencesort
# register the test suite in setup.py
Patch0: python-zope-sequencesort-3.4.0-run_tests.patch

BuildRequires: python2-devel
BuildRequires: python-setuptools
# for tests
BuildRequires: python-zope-testing

%description
This package provides a very advanced sequence sorting feature.

%prep
%setup -q -n %{modname}-%{version}
%patch0 -p1 -b .run_tests

%build
%{__python} setup.py build

%install
%{__python} setup.py install --root=%{buildroot}

%check
%{__python} setup.py test

%files
%defattr(-,root,root,-)
# LICENSE will be included in next upstream release
%doc CHANGES.txt README.txt
# Co-own %%{python_sitelib}/zope/
%dir %{python_sitelib}/zope/
%{python_sitelib}/zope/sequencesort/
%exclude %{python_sitelib}/zope/sequencesort/tests/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sun Sep 19 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.0-2
- Spec cleaned up
- Exclude the tests from installation
- Co-own %%{python_sitelib}/zope/
- Requires: python-zope-filesystem removed
- Add a small patch and %%check section and run tests

* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.0-1
- Initial packaging
