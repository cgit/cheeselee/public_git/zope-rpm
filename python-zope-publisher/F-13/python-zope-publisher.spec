%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.publisher
%global version 3.12.3
%global release 1

Summary: Publishing Python objects on the web
Name: python-zope-publisher
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.publisher

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-browser
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-contenttype
Requires: python-zope-event
Requires: python-zope-exceptions
Requires: python-zope-i18n
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-proxy
Requires: python-zope-security

%description
zope.publisher allows you to publish Python objects on the web.  It
has support for plain HTTP/WebDAV clients, web browsers as well as
XML-RPC and FTP clients.  Input and output streams are represented by
request and response objects which allow for easy client interaction
from Python.  The behavior of the publisher is geared towards WSGI
compatibility.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/publisher/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%doc src/zope/publisher/*.txt
%{python_sitelib}/zope/publisher/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.12.3-1
- Initial packaging
