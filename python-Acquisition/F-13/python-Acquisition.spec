%include %{_rpmconfigdir}/macros.python
%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
%global modname Acquisition
%global version 2.13.3
%global release 2

Summary: Allowing objects to obtain attributes from their containment hierarchy
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
License: ZPLv2.1
Source0: http://pypi.python.org/packages/source/A/%{modname}/%{modname}-%{version}.zip
Group: Development/Libraries
URL: http://pypi.python.org/pypi/Acquisition

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-ExtensionClass
Requires: python-zope-interface

%description
Acquisition provides a powerful way to dynamically share information
between objects. Zope 2 uses acquisition for a number of its key
features including security, object publishing, and DTML variable
lookup. Acquisition also provides an elegant solution to the problem
of circular references for many classes of problems. While acquisition
is powerful, you should take care when using acquisition in your
applications. The details can get complex, especially with the
differences between acquiring from context and acquiring from
containment.

%package devel
Summary:  Developer files for %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: python2-devel
Requires: python-ExtensionClass-devel
BuildArch: noarch
%description devel
Files for developing applications using %{name}.

%prep
%setup -q -n %{modname}-%{version}

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# remove contained source file(s)
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv
# deal with header file(s)
mkdir -p $RPM_BUILD_ROOT%{_includedir}/python%{py_ver}/%{modname}/
mv $RPM_BUILD_ROOT%{python_sitearch}/%{modname}/%{modname}.h \
   $RPM_BUILD_ROOT%{_includedir}/python%{py_ver}/%{modname}/

rm -f $RPM_BUILD_ROOT%{python_sitearch}/%{modname}/README.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt COPYRIGHT.txt LICENSE.txt src/Acquisition/README.txt
%{python_sitearch}/%{modname}/
%{python_sitearch}/%{modname}-*.egg-info

%files devel
%defattr(-,root,root,-)
%{_includedir}/python%{py_ver}/%{modname}/

%changelog
* Tue Jun 22 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.13.3-2
- BR: python-ExtensionClass-devel removed

* Tue Jun 15 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.13.3-1
- Initial packaging
