%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.zcmlfiles

Summary: Zope application server ZCML files
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.7.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.zcmlfiles

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-annotation
Requires: python-zope-component
Requires: python-zope-componentvocabulary
Requires: python-zope-copypastemove
Requires: python-zope-dublincore
Requires: python-zope-formlib
Requires: python-zope-i18n
Requires: python-zope-location
Requires: python-zope-publisher
Requires: python-zope-size
Requires: python-zope-traversing
Requires: python-zope-app-applicationcontrol
Requires: python-zope-app-appsetup
Requires: python-zope-app-basicskin
Requires: python-zope-app-broken
Requires: python-zope-app-component
Requires: python-zope-app-container
Requires: python-zope-app-content
Requires: python-zope-app-dependable
Requires: python-zope-app-error
Requires: python-zope-app-exception
Requires: python-zope-app-folder
Requires: python-zope-app-form
Requires: python-zope-app-generations
Requires: python-zope-app-http
Requires: python-zope-app-i18n
Requires: python-zope-app-locales >= 3.6.0
Requires: python-zope-app-pagetemplate
Requires: python-zope-app-principalannotation
Requires: python-zope-app-publication
Requires: python-zope-app-publisher
Requires: python-zope-app-rotterdam
Requires: python-zope-app-schema
Requires: python-zope-app-security
Requires: python-zope-app-wsgi
Requires: python-zope-app-zopeappgenerations

%description
Zope application server ZCML files

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.7.0-1
- Initial packaging
