%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.debug
%global version 3.4.1
%global release 1

Summary: Zope Debug Mode
Name: python-zope-app-debug
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://cheeseshop.python.org/pypi/zope.app.debug

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-publisher
Requires: python-zope-app-appsetup
Requires: python-zope-app-publication

%description
This package provides a debugger for the Zope publisher. After Zope is
instantiated, it drops the user into an interactive Python shell where the
full Zope environment and the database root are available.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/app/debug/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jun 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.1-1
- Initial packaging
