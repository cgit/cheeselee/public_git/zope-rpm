%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.appsetup
%global version 3.14.0
%global release 1

Summary: Zope app setup helper
Name: python-zope-app-appsetup
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.appsetup

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-ZODB3
Requires: python-zope-app-publication
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-container
Requires: python-zope-error
Requires: python-zope-event
Requires: python-zope-interface
Requires: python-zope-processlifetime
Requires: python-zope-security
Requires: python-zope-session
Requires: python-zope-site
Requires: python-zope-traversing

%description
This package provides application setup helpers for the Zope3 appserver.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_bindir}/debug
#rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/app/appsetup/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
#%doc src/zope/app/appsetup/*.txt
%{python_sitelib}/zope/app/appsetup/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jun 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.14.0-1
- Initial packaging
