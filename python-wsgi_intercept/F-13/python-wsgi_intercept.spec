%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname wsgi_intercept

Summary: Installs a WSGI application in place of a real URI for testing
Name: python-%{modname}
Version: 0.4
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: MIT
Group: Development/Libraries
BuildArch: noarch
URL: http://code.google.com/p/wsgi-intercept/

BuildRequires: python2-devel
BuildRequires: python-setuptools

%description
Testing a WSGI application normally involves starting a server at a local host
and port, then pointing your test code to that address.  Instead, this library
lets you intercept calls to any specific host/port combination and redirect them
into a `WSGI application`_ importable by your test program.  Thus, you can avoid
spawning multiple processes or threads to test your Web app.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build
sed -i -e '/^#! *\//, 1d' %{modname}/test/test_{wsgi_{urllib2,compliance},mechanoid,webtest,httplib2,webunit}.py

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt AUTHORS.txt CHANGELOG.txt README.txt
%{python_sitelib}/%{modname}/
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Thu Jul  8 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.4-1
- Initial packaging
