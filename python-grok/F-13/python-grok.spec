%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grok

Summary: Simple and interesting web framework based on Zope Toolkit
Name: python-%{modname}
Version: 1.1.1
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://grok.zope.org

BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildRequires: python-sphinx
Requires: python-setuptools
Requires: python-ZODB3
Requires: python-grokcore-annotation >= 1.1
Requires: python-grokcore-component < 2.0
Requires: python-grokcore-formlib >= 1.4
Requires: python-grokcore-security >= 1.1
Requires: python-grokcore-site
Requires: python-grokcore-content
Requires: python-grokcore-view >= 1.12
Requires: python-grokcore-viewlet >= 1.3
Requires: python-martian < 0.12
Requires: pytz
Requires: python-simplejson
Requires: python-z3c-autoinclude
Requires: python-z3c-flashmessage
Requires: python-z3c-testsetup
Requires: python-zc-catalog
Requires: python-zope-annotation
Requires: python-zope-app-publication
Requires: python-zope-app-publisher
Requires: python-zope-app-renderer
Requires: python-zope-app-zcmlfiles
Requires: python-zope-catalog
Requires: python-zope-component
Requires: python-zope-container
Requires: python-zope-contentprovider
Requires: python-zope-copypastemove
Requires: python-zope-event
Requires: python-zope-exceptions
Requires: python-zope-formlib
Requires: python-zope-i18n
Requires: python-zope-interface
Requires: python-zope-intid
Requires: python-zope-keyreference
Requires: python-zope-lifecycleevent
Requires: python-zope-location
Requires: python-zope-password
Requires: python-zope-pluggableauth
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-securitypolicy
Requires: python-zope-site
Requires: python-zope-size
Requires: python-zope-traversing
Requires: python-zope-app-appsetup
Requires: python-zope-app-pagetemplate
Requires: python-zope-app-security
Requires: python-zope-app-testing
Requires: python-zope-deprecation
Requires: python-zope-dottedname
Requires: python-zope-hookable
Requires: python-zope-i18nmessageid
Requires: python-zope-pagetemplate
Requires: python-zope-testbrowser
Requires: python-zope-proxy
Requires: python-zope-viewlet

%description
Grok makes it easier to get started with a Zope 3 web application.

Grok uses the Component Architecture and builds on Zope 3 concepts
like content objects (models), views, and adapters.  Its simplicity
lies in using "convention over configuration" and "sensible
defaults" when wiring components together.  That means neither a
configuration language like ZCML nor a lot of repetition are needed to
create a web application with grok.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build
python -c "import sys; sys.path.append('grokdocs/src'); from grokdocs import grokdocs; grokdocs.render();"
rm -fr build/html/{.buildinfo,.doctrees/,_sources/}

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt TODO.txt CREDITS.txt COPYRIGHT.txt
%doc build/html/
%{python_sitelib}/%{modname}/
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Thu Jul  8 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.1.1-1
- Initial packaging
