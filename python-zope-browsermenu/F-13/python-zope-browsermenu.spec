%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.browsermenu
%global version 3.9.1
%global release 1

Summary: Browser menu implementation for Zope
Name: python-zope-browsermenu
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.browsermenu/

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-browser
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-pagetemplate
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-traversing

%description
This package provides an implementation of browser menus and ZCML directives
for configuring them.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{python_sitelib}/zope/browsermenu/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt src/zope/browsermenu/README.txt
%{python_sitelib}/zope/browsermenu/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.1-1
- Initial packaging
