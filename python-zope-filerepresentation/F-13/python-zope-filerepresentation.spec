%global modname zope.filerepresentation

Summary: File-system Representation Interfaces
Name: python-zope-filerepresentation
Version: 3.6.0
Release: 2%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.filerepresentation

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-interface

%description
The interfaces defined here are used for file-system and file-system-like
representations of objects, such as file-system synchronization, FTP, PUT, and
WebDAV.

%prep
%setup -q -n %{modname}-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install --root=$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
# Co-own %%{python_sitelib}/zope/
%dir %{python_sitelib}/zope/
%{python_sitelib}/zope/filerepresentation/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Tue Sep 14 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.6.0-2
- Requires: python-zope-filesystem and python-setuptools removed
- Co-own %%{python_sitelib}/zope/
- Spec cleaned up

* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.6.0-1
- Initial packaging
