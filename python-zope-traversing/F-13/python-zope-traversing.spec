%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.traversing
%global version 3.12.1
%global release 1

Summary: Resolving paths in the object hierarchy
Name: python-zope-traversing
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.traversing

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-component
Requires: python-zope-i18n
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-proxy
Requires: python-zope-publisher
Requires: python-zope-security
Requires: python-zope-location

%description
This package provides adapters for resolving object paths by traversing an
object hierarchy.  This also includes support for traversal namespaces as well
as computing URLs.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/traversing/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.12.1-1
- Initial packaging
