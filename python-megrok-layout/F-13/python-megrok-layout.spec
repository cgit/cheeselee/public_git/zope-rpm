%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname megrok.layout

Summary: A layout component package for zope3 and Grok
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 1.1.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: GPL
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/megrok.layout

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-grokcore-component
Requires: python-grokcore-formlib
Requires: python-grokcore-message
Requires: python-grokcore-security
Requires: python-grokcore-view >= 1.13.1
Requires: python-martian
Requires: python-setuptools
Requires: python-zope-component >= 3.9.1
Requires: python-zope-interface
Requires: python-zope-publisher

%description
The megrok.layout package provides a simple way to write view
components which can be included into a defined layout. It turns
around two main components : the Page and the Layout.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc docs/*
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jul  9 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.1.0-1
- Initial packaging
