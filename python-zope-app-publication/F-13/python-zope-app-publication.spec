%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.publication
%global version 3.11.1
%global release 1

Summary: Zope publication
Name: python-zope-app-publication
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.publication

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-ZODB3
Requires: python-zope-authentication
Requires: python-zope-component
Requires: python-zope-error
Requires: python-zope-browser
Requires: python-zope-location
Requires: python-zope-publisher
Requires: python-zope-traversing

%description
Publication and traversal components.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/app/publication/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%doc src/zope/app/publication/*.txt
%{python_sitelib}/zope/app/publication/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.11.1-1
- Initial packaging
