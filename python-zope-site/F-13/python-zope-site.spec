%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.site
%global version 3.9.1
%global release 1

Summary: Local registries for zope component architecture
Name: python-zope-site
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.site

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-annotation
Requires: python-zope-container
Requires: python-zope-security
Requires: python-zope-component
Requires: python-zope-event
Requires: python-zope-interface
Requires: python-zope-lifecycleevent
Requires: python-zope-location

%description
This package provides a local and persistent site manager
implementation, so that one can register local utilities and
adapters. It uses local adapter registries for its adapter and utility
registry. The module also provides some facilities to organize the
local software and ensures the correct behavior inside the ZODB.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/site/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%doc src/zope/site/*.txt
%{python_sitelib}/zope/site/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.1-1
- Initial packaging
