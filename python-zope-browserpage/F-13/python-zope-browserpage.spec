%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.browserpage
%global version 3.12.2
%global release 1

Summary: ZCML directives for configuring browser views for Zope
Name: python-zope-browserpage
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.browserpage/

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-pagetemplate
Requires: python-zope-component >= 3.7
Requires: python-zope-configuration
Requires: python-zope-interface
Requires: python-zope-publisher >= 3.8
Requires: python-zope-schema
Requires: python-zope-security 
Requires: python-zope-traversing

%description
This package provides ZCML directives for configuring browser views.
More specifically it defines the following ZCML directives:

 * browser:page
 * browser:pages
 * browser:view

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
%{python_sitelib}/zope/browserpage/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.12.2-1
- Initial packaging
