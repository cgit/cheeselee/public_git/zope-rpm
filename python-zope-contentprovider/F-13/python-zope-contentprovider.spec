%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.contentprovider
%global version 3.7.2
%global release 1

Summary: Content Provider Framework for Zope Templates
Name: python-zope-contentprovider
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.contentprovider

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-component
Requires: python-zope-event
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-tales

%description
This package provides a framework to develop componentized Web GUI
applications. Instead of describing the content of a page using a single
template or static system of templates and METAL macros, content provider
objects are dynamically looked up based on the setup/configuration of the
application.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/contentprovider/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt COPYRIGHT.txt
%doc src/zope/contentprovider/*.txt
%{python_sitelib}/zope/contentprovider/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.7.2-1
- Initial packaging
