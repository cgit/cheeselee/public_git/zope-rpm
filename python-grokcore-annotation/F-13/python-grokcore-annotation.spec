%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grokcore.annotation
%global version 1.2

Summary: Grok-like configuration for Zope annotations
Name: python-grokcore-annotation
Version: %{version}
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/g/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://grok.zope.org

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-ZODB3
Requires: python-grokcore-component
Requires: python-martian
Requires: python-setuptools
Requires: python-zope-annotation
Requires: python-zope-component
Requires: python-zope-container
Requires: python-zope-interface

%description
This package provides a support to simplify the use of annotations in
Zope.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt
%{python_sitelib}/grokcore/annotation/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.2-1
- Initial packaging
