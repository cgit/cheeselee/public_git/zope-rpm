%global modname zope.processlifetime

Summary: Zope process lifetime events
Name: python-zope-processlifetime
Version: 1.0
Release: 2%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.processlifetime

BuildRequires: python2-devel
BuildRequires: python-setuptools

%description
This package provides interfaces / implementations for events relative to
the lifetime of a server process (startup, database opening, etc.)

%prep
%setup -q -n %{modname}-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install --root=%{buildroot}

%check
%{__python} setup.py test

%files
%defattr(-,root,root,-)
# LICENSE will be included in next upstream release
%doc CHANGES.txt README.txt
# Co-own %%{python_sitelib}/zope/
%dir %{python_sitelib}/zope/
%{python_sitelib}/zope/processlifetime/
%exclude %{python_sitelib}/zope/processlifetime/tests.py*
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sat Sep 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.0-2
- Spec cleaned up
- Requires: python-zope-filesystem and python-setuptools removed
- Exclude the tests from installation
- Co-own %%{python_sitelib}/zope/
- Add %%check section and run tests

* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.0-1
- Initial packaging
