%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.copypastemove

Summary: Copy, Paste and Move support for content components
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.6.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.copypastemove

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-annotation
Requires: python-zope-component
Requires: python-zope-container
Requires: python-zope-copy
Requires: python-zope-event
Requires: python-zope-exceptions
Requires: python-zope-interface
Requires: python-zope-lifecycleevent
Requires: python-zope-location

%description
This package provides Copy, Paste and Move support for content
components in Zope.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jul  7 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.6.0-1
- Initial packaging
