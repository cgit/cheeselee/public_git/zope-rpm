%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname z3c.recipe.i18n

Summary: Zope3 egg based i18n locales extraction recipes
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 0.7.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/z3c.recipe.i18n

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem >= 1-10
Requires: python-setuptools
Requires: python-zc-buildout
Requires: python-zc-recipe-egg
Requires: python-zope-app-appsetup
Requires: python-zope-app-locales
Requires: python-zope-configuration
# from "extract" extra of zope.app.locales
Requires: python-zope-tal
Requires: python-zope-app-applicationcontrol

%description
This Zope 3 recipes offers different tools which allows to extract i18n 
translation messages from egg based packages.

%prep
%setup -q -n %{modname}-%{version}
sed -i 's/\r//' src/z3c/recipe/i18n/i18ncompile.py
sed -i -e '/^#! *\//, 1d' src/z3c/recipe/i18n/i18n{extract,compile,stats,mergeall}.py

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sat Jul 10 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.7.0-1
- Initial packaging
