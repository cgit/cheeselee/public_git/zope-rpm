%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.pagetemplate

Summary: PageTemplate integration for Zope 3
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.11.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.pagetemplate

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-browserpage >= 3.12.0
Requires: python-zope-component 
Requires: python-zope-configuration
Requires: python-zope-dublincore
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-pagetemplate >= 3.5.0
Requires: python-zope-publisher
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-size
Requires: python-zope-tales
Requires: python-zope-traversing
# from "untrustedpython" extra of zope.security
Requires: python-RestrictedPython

%description
This package integrates the Page Template templating system into the Zope 3
application server.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jul  7 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.11.0-1
- Initial packaging
