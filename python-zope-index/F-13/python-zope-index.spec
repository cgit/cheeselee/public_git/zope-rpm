%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
%global modname zope.index

Summary: Indices for using with catalog like text, field, etc
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.6.1
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/zope.index

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-ZODB3
Requires: python-zope-interface

%description
The 'zope.index' package provides several indices for the Zope
catalog.  These include:

* a field index (for indexing orderable values),

* a keyword index,

* a topic index,

* a text index (with support for lexicon, splitter, normalizer, etc.)

%prep
%setup -q -n %{modname}-%{version}

# remove some shebangs
sed -i -e '/^#! *\//, 1d' src/zope/index/text/tests/{mhindex,wordstats,hs-tool}.py

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# remove contained source file(s)
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitearch}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitearch}/%{modname}-*.egg-info
%{python_sitearch}/%{modname}-*-nspkg.pth


%changelog
* Fri Jul  9 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.6.1-1
- Initial packaging
