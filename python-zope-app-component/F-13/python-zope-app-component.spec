%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.component

Summary: Local Zope Component Support
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.8.4
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.component

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-site
Requires: python-zope-annotation
Requires: python-zope-app-container
Requires: python-zope-app-pagetemplate
Requires: python-zope-cachedescriptors
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-deferredimport
Requires: python-zope-deprecation
Requires: python-zope-event
Requires: python-zope-exceptions
Requires: python-zope-filerepresentation
Requires: python-zope-formlib
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-lifecycleevent
Requires: python-zope-publisher >= 3.12.0
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-traversing
Requires: python-zope-componentvocabulary
Requires: python-ZODB3
# below are from zope.component 'hook' extra
Requires: python-zope-hookable


%description
This package provides various ZCML directives (view, resource) and a
user interface related to local component management.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Mon Jul  5 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.8.4-1
- Initial packaging
