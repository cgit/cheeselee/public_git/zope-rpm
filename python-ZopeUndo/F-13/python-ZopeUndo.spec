%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname ZopeUndo
%global version 2.12.0
%global release 1

Summary: ZODB undo support for Zope2
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/Z/%{modname}/%{modname}-%{version}.zip
BuildArch: noarch
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/ZopeUndo

BuildRequires: python2-devel
BuildRequires: python-setuptools

%description
This package is used to support the Prefix object that Zope 2 uses for the
undo log. It is a separate package only to aid configuration management.

This package is included in Zope 2. It can be used in a ZEO server to allow
it to support Zope 2's undo log , without pulling in all of Zope 2.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%{modname}/
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Tue Jun 15 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.12.0-1%{?dist}
- Initial packaging
