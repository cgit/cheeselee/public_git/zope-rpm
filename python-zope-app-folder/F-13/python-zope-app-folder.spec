%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.folder

Summary: Folder and Site -- Zope 3 Content Components
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.5.1
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.folder

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-zope-container
Requires: python-zope-site
Requires: python-zope-datetime
Requires: python-zope-dublincore
Requires: python-zope-event
Requires: python-zope-interface
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-traversing
Requires: python-zope-app-authentication
Requires: python-zope-app-component
Requires: python-zope-app-content
Requires: python-zope-app-security
Requires: python-ZODB3

%description
This packages provides the standard Folder content type for Zope 3. It also
implements the root folder of a Zope application. Folders can also be
converted to sites, which allow one to register components locally.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jul  7 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.1-1
- Initial packaging
