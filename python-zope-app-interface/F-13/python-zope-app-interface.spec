%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.interface

Summary: Zope Interface Extensions
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 3.5.2
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://cheeseshop.python.org/pypi/zope.app.interface

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: python-ZODB3
Requires: python-zodbcode
Requires: python-zope-componentvocabulary
Requires: python-zope-security

%description
This package provides several extensions to Zope interfaces, such as a
persistent implementation, interface type queries, and a vocabulary of all
registered interfaces of the system (or of a particular type).

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jul  8 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.2-1
- Initial packaging
