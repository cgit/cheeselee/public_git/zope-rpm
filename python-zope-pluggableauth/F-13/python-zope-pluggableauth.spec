%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.pluggableauth

Summary: Pluggable Authentication Utility
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 1.0.3
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.pluggableauth

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-ZODB3
Requires: python-setuptools
Requires: python-zope-authentication
Requires: python-zope-component
Requires: python-zope-container
Requires: python-zope-event
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-publisher >= 3.12
Requires: python-zope-schema
Requires: python-zope-security
Requires: python-zope-session
Requires: python-zope-site
Requires: python-zope-traversing

%description
Based on zope.authentication, this package provides a flexible and
pluggable authentication utility. Several common plugins are provided.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt ZPL.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sat Jul 10 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.0.3-1
- Initial packaging
