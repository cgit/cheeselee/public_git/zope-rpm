%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zLOG
%global version 2.11.1
%global release 1

Summary: A general logging facility
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://cheeseshop.python.org/pypi/zLOG

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-ZConfig

%description
This package provides a general logging facility that, at this point,
is just a small shim over Python's logging module.  Therefore, unless
you need to support a legacy package from the Zope 2 world, you're
probably better off using Python's logging module.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc TODO.txt
%{python_sitelib}/%{modname}/
%{python_sitelib}/%{modname}-*.egg-info


%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.11.1-1
- Initial packaging
