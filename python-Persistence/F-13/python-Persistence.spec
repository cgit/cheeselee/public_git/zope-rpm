%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
%global modname Persistence
%global version 2.13.2
%global release 1

Summary: Persistent ExtensionClass
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/P/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/Persistence

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-ExtensionClass
Requires: python-ZODB3

%description
This package provides a variant of the persistent base class that's an
ExtensionClass. Unless you need ExtensionClass semantics, you probably want to
use persistent.Persistent from ZODB3.

%prep
%setup -q -n %{modname}-%{version}

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# remove contained source file(s)
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt  COPYRIGHT.txt  LICENSE.txt README.txt
%{python_sitearch}/%{modname}/
%{python_sitearch}/%{modname}-*.egg-info

%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.13.2-1
- Initial packaging
