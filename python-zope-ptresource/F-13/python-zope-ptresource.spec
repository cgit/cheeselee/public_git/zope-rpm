%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.ptresource
%global version 3.9.0
%global release 1

Summary: Page template resource plugin for zope.browserresource
Name: python-zope-ptresource
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.ptresource/

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-browserresource
Requires: python-zope-interface
Requires: python-zope-pagetemplate
Requires: python-zope-publisher
Requires: python-zope-security

%description
This package provides a "page template" resource class, a resource which
content is processed with Zope Page Templates engine before returning to
client.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/ptresource/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.0-1
- Initial packaging
