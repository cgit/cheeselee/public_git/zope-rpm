%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.session
%global version 3.9.2
%global release 1

Summary: Client identification and sessions for Zope
Name: python-zope-session
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.session

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-ZODB3
Requires: python-zope-component
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-location
Requires: python-zope-publisher
Requires: python-zope-minmax

%description
This package provides interfaces for client identification and session
support and their implementations for zope.publisher's request objects.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
#rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/session/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
#%doc src/zope/session/*.txt
%{python_sitelib}/zope/session/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jun 18 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.9.2-1
- Initial packaging
