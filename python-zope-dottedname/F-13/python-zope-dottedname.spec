%global modname zope.dottedname

Summary: Resolver for Python dotted names
Name: python-zope-dottedname
Version: 3.4.6
Release: 2%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.dottedname

# register the test suite, applied in upstream svn116336
Patch0: python-zope-dottedname-3.4.6-run-tests.patch
 

BuildRequires: python2-devel
BuildRequires: python-setuptools
# for tests, should be removed in the next upstream release
BuildRequires: python-zope-testing

%description
The zope.dottedname module provides one function, 'resolve' that
resolves strings containing dotted names into the appropriate Python
object.

%prep
%setup -q -n %{modname}-%{version}
%patch0 -p2 -b .run_tests

%build
%{__python} setup.py build

%install
%{__python} setup.py install --root=$RPM_BUILD_ROOT

%check
%{__python} setup.py test

%files
%defattr(-,root,root,-)
%doc CHANGES.txt
# Co-own %%{python_sitelib}/zope/
%dir %{python_sitelib}/zope/
%{python_sitelib}/zope/dottedname/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Mon Sep 13 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.6-2
- Requires: python-zope-filesystem and python-setuptools removed
- Add %%check section and run tests
- Add python-zope-dottedname-3.4.6-run-tests.patch to register the test_suite
- Co-own %%{python_sitelib}/zope/
- Don't move the text files
- Spec cleaned up

* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.4.6-1
- Initial packaging
