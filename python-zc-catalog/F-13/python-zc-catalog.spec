%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zc.catalog

Summary: Extensions to the Zope 3 Catalog
Name: python-%(echo %{modname} | sed -r 's|\.|-|')
Version: 1.4.4
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zc.catalog

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-ZODB3
Requires: pytz
Requires: python-setuptools
Requires: python-zope-catalog
Requires: python-zope-component
Requires: python-zope-container
Requires: python-zope-i18nmessageid
Requires: python-zope-index >= 3.5.1
Requires: python-zope-interface
Requires: python-zope-publisher >= 3.12
Requires: python-zope-schema
Requires: python-zope-security

%description
zc.catalog is an extension to the Zope 3 catalog, Zope 3's indexing
and search facility. zc.catalog contains a number of extensions to the
Zope 3 catalog, such as some new indexes, improved globbing and
stemming support, and an alternative catalog implementation.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt ZopePublicLicense.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Tue Jul  6 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.4.4-1
- Initial packaging
