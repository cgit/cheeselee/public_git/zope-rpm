%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grokcore.startup

Summary: Paster support for Grok projects
Name: python-%(echo %{modname} | sed -r 's|\.|-|g')
Version: 1.0
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://grok.zope.org

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-setuptools
Requires: python-zdaemon
Requires: python-zope-component
Requires: python-zope-security
Requires: python-zope-publisher
Requires: python-zope-dottedname
Requires: python-zope-app-wsgi
Requires: python-zope-app-debug

%description
This package provides elements for starting a Grok project with paster and WSGI.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt CREDITS.txt COPYRIGHT.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|g')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jul  8 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.0-1
- Initial packaging
