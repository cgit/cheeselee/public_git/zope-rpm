%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.pagetemplate

Summary: Zope Page Templates
Name: python-zope-pagetemplate
Version: 3.5.2
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.pagetemplate

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-interface
Requires: python-zope-component
Requires: python-zope-security 
Requires: python-zope-tales
Requires: python-zope-tal
Requires: python-zope-i18n
Requires: python-zope-i18nmessageid
Requires: python-zope-traversing

%description
Page Templates provide an elegant templating mechanism that achieves a
clean separation of presentation and application logic while allowing
for designers to work with templates in their visual editing tools
(FrontPage, Dreamweaver, GoLive, etc.).

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt COPYRIGHT.txt LICENSE.txt README.txt
%{python_sitelib}/zope/pagetemplate/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Fri Jul  9 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.2-1
- Update to 3.5.2
- Don't move the text files

* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.1-1
- Initial packaging
