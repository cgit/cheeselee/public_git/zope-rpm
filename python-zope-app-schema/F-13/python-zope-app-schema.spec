%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.app.schema
%global version 3.5.0
%global release 1

Summary: Component Architecture based Vocabulary Registry
Name: python-zope-app-schema
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://pypi.python.org/pypi/zope.app.schema

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-zope-component
Requires: python-zope-interface
Requires: python-zope-schema

%description
This package provides a component architecture based vocabulary registry.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{python_sitelib}/zope/app/schema/*.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt
%doc src/zope/app/schema/*.txt
%{python_sitelib}/zope/app/schema/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Thu Jun 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 3.5.0-1
- Initial packaging
