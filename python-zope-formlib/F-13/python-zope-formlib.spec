%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname zope.formlib
%global version 4.0.3
%global release 1

Summary: Form generation and validation library for Zope
Name: python-zope-formlib
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildArch: noarch
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/zope.formlib

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-zope-filesystem
Requires: python-setuptools
Requires: pytz
Requires: python-zope-app-form
Requires: python-zope-browser >= 1.1
Requires: python-zope-browserpage
Requires: python-zope-component
Requires: python-zope-event
Requires: python-zope-i18n
Requires: python-zope-i18nmessageid
Requires: python-zope-interface
Requires: python-zope-lifecycleevent
Requires: python-zope-publisher
Requires: python-zope-schema >= 3.5.1
Requires: python-zope-security
Requires: python-zope-traversing
Requires: python-zope-datetime

%description
Forms are web components that use widgets to display and input data.
Typically a template displays the widgets by accessing an attribute or
method on an underlying class.

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{python_sitelib}/zope/formlib/TODO.txt

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt COPYRIGHT.txt src/zope/formlib/TODO.txt
%{python_sitelib}/zope/formlib/
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Wed Jun 16 2010 Robin Lee <robinlee.sysu@gmail.com> - 4.0.3-1
- Initial packaging
