%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif
%global modname grokcore.component

Summary: Grok-like configuration for basic components
Name: python-%(echo %{modname} | sed -r 's|\.|-|')
Version: 1.8
Release: 1%{?dist}
Source0: http://pypi.python.org/packages/source/%(echo %{modname} | sed -r 's|^(.).*|\1|')/%{modname}/%{modname}-%{version}.tar.gz
License: ZPLv2.1
Group: Development/Libraries
BuildArch: noarch
URL: http://grok.zope.org

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-grok-filesystem
Requires: python-setuptools
Requires: python-martian >= 0.10
Requires: python-zope-component
Requires: python-zope-configuration
Requires: python-zope-interface
Requires: python-zope-testing

%description
This package provides base classes of basic component types for the
Zope Component Architecture, as well as means for configuring and
registering them directly in Python (without ZCML).

%prep
%setup -q -n %{modname}-%{version}

%build
python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt CHANGES.txt README.txt INSTALL.txt TODO.txt CREDITS.txt COPYRIGHT.txt
%{python_sitelib}/%(echo %{modname} | sed -r 's|\.|/|')
%{python_sitelib}/%{modname}-*.egg-info
%{python_sitelib}/%{modname}-*-nspkg.pth


%changelog
* Sun Jul  4 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.8-1
- Initial packaging
