%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
%global modname MultiMapping
%global version 2.13.0
%global release 1

Summary: Special MultiMapping objects used in Zope2
Name: python-%{modname}
Version: %{version}
Release: %{release}%{?dist}
Source0: http://pypi.python.org/packages/source/M/%{modname}/%{modname}-%{version}.zip
License: ZPLv2.1
Group: Development/Libraries
URL: http://pypi.python.org/pypi/MultiMapping

BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires: python-ExtensionClass

%description
MultiMapping provides special objects used in some Zope2 internals like ZRDB.

%prep
%setup -q -n %{modname}-%{version}

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python setup.py install --root=$RPM_BUILD_ROOT
# remove contained source file(s)
find $RPM_BUILD_ROOT -name '*.c' -type f -print0 | xargs -0 rm -fv

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitearch}/%{modname}/
%{python_sitearch}/%{modname}-*.egg-info


%changelog
* Tue Jun 15 2010 Robin Lee <robinlee.sysu@gmail.com> - 2.13.0-1
- Initial packaging
